package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.Card;
import com.ruoyi.system.mapper.CardMapper;
import com.ruoyi.system.service.ICardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 校园卡信息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-07-03
 */
@Service
public class CardServiceImpl implements ICardService
{
    @Autowired
    private CardMapper cardMapper;
    /**
     * 查询校园卡信息
     *
     * @param cardNum 校园卡信息主键
     * @return 校园卡信息
     */
    @Override
    public Card selectCardByCardNum(Long cardNum)
    {
        return cardMapper.selectCardByCardNum(cardNum);
    }
    /**
     * 根据拥有者查询校园卡信息
     *
     * @param cardName 校园卡拥有者
     * @return 校园卡信息
     */
    @Override
    public Card selectCardBYCardName(String cardName) {
        return cardMapper.selectCardByCardName(cardName);
    }
    /**
     * 根据卡号查询校园卡信息
     *
     * @param cardId 校园卡号
     * @return 校园卡信息
     */
    public Card selectCardBYCardId(String cardId) {
        return cardMapper.selectCardByCardId(cardId);
    }
    /**
     * 查询校园卡信息列表
     *
     * @param card 校园卡信息
     * @return 校园卡信息
     */
    @Override
    public List<Card> selectCardList(Card card)
    {
        return cardMapper.selectCardList(card);
    }

    /**
     * 新增校园卡信息
     *
     * @param card 校园卡信息
     * @return 结果
     */
    @Override
    public int insertCard(Card card)
    {
        return cardMapper.insertCard(card);
    }

    /**
     * 修改校园卡信息
     *
     * @param card 校园卡信息
     * @return 结果
     */
    @Override
    public int updateCard(Card card)
    {
        return cardMapper.updateCard(card);
    }

    /**
     * 批量删除校园卡信息
     *
     * @param cardNums 需要删除的校园卡信息主键
     * @return 结果
     */
    @Override
    public int deleteCardByCardNums(String cardNums)
    {
        return cardMapper.deleteCardByCardNums(Convert.toStrArray(cardNums));
    }

    /**
     * 删除校园卡信息信息
     *
     * @param cardNum 校园卡信息主键
     * @return 结果
     */
    @Override
    public int deleteCardByCardNum(Long cardNum)
    {
        return cardMapper.deleteCardByCardNum(cardNum);
    }
    /**
     * 查询校园卡id是否唯一
     *
     * @param card 校园卡信息
     * @return 结果
     */
    @Override
    public boolean checkCardIdUnique(Card card) {
        Long cardNum= StringUtils.isNull(card.getCardNum())? -1L : card.getCardNum();
        Card info= cardMapper.checkCardIdUnique(card.getCardId());
        if(StringUtils.isNotNull(info)&& info.getCardNum().longValue()!=cardNum.longValue())
        {
            return false;
        }
        return true;
    }
    /**
     * 查询账号是否已经有了校园卡
     *
     * @param card 校园卡信息
     * @return 结果
     */
    @Override
    public boolean checkCardNameUnique(Card card) {
        Long cardNum= StringUtils.isNull(card.getCardNum())? -1L : card.getCardNum();
        Card info= cardMapper.checkCardNameUnique(card.getCardName());
        if(StringUtils.isNotNull(info)&& info.getCardNum().longValue()!=cardNum.longValue())
        {
            return false;
        }
        return true;
    }
}
