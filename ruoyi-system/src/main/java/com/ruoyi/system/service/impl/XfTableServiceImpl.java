package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.system.domain.XfTable;
import com.ruoyi.system.mapper.XfTableMapper;
import com.ruoyi.system.service.IXfTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 消费Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-06
 */
@Service
public class XfTableServiceImpl implements IXfTableService 
{
    @Autowired
    private XfTableMapper xfTableMapper;

    /**
     * 查询消费
     * 
     * @param cid 消费主键
     * @return 消费
     */
    @Override
    public XfTable selectXfTableByCid(Long cid)
    {
        return xfTableMapper.selectXfTableByCid(cid);
    }

    /**
     * 查询消费列表
     * 
     * @param xfTable 消费
     * @return 消费
     */
    @Override
    public List<XfTable> selectXfTableList(XfTable xfTable)
    {
        return xfTableMapper.selectXfTableList(xfTable);
    }

    /**
     * 新增消费
     * 
     * @param xfTable 消费
     * @return 结果
     */
    @Override
    public int insertXfTable(XfTable xfTable)
    {
        return xfTableMapper.insertXfTable(xfTable);
    }

    /**
     * 修改消费
     * 
     * @param xfTable 消费
     * @return 结果
     */
    @Override
    public int updateXfTable(XfTable xfTable)
    {
        return xfTableMapper.updateXfTable(xfTable);
    }

    /**
     * 批量删除消费
     * 
     * @param cids 需要删除的消费主键
     * @return 结果
     */
    @Override
    public int deleteXfTableByCids(String cids)
    {
        return xfTableMapper.deleteXfTableByCids(Convert.toStrArray(cids));
    }

    /**
     * 删除消费信息
     * 
     * @param cid 消费主键
     * @return 结果
     */
    @Override
    public int deleteXfTableByCid(Long cid)
    {
        return xfTableMapper.deleteXfTableByCid(cid);
    }
}
