package com.ruoyi.system.service;

import com.ruoyi.system.domain.Card;

import java.util.List;

/**
 * 校园卡信息Service接口
 *
 * @author ruoyi
 * @date 2023-07-03
 */
public interface ICardService
{
    /**
     * 查询校园卡信息
     *
     * @param cardNum 校园卡信息主键
     * @return 校园卡信息
     */
    public Card selectCardByCardNum(Long cardNum);

    /**
     * 查询校园卡信息列表
     *
     * @param card 校园卡信息
     * @return 校园卡信息集合
     */
    public List<Card> selectCardList(Card card);
    /**
     * 根据拥有者查询校园卡信息
     *
     * @param cardName 校园卡拥有者
     * @return 校园卡信息
     */
    public Card selectCardBYCardName(String cardName);
    /**
     * 根据卡号查询校园卡信息
     *
     * @param cardId 校园卡号
     * @return 校园卡信息
     */
    public Card selectCardBYCardId(String cardId);
    /**
     * 新增校园卡信息
     *
     * @param card 校园卡信息
     * @return 结果
     */
    public int insertCard(Card card);

    /**
     * 修改校园卡信息
     *
     * @param card 校园卡信息
     * @return 结果
     */
    public int updateCard(Card card);

    /**
     * 批量删除校园卡信息
     *
     * @param cardNums 需要删除的校园卡信息主键集合
     * @return 结果
     */
    public int deleteCardByCardNums(String cardNums);

    /**
     * 删除校园卡信息信息
     *
     * @param cardNum 校园卡信息主键
     * @return 结果
     */
    public int deleteCardByCardNum(Long cardNum);
    /**
     * 查询校园卡id是否唯一
     *
     * @param card 校园卡信息
     * @return 结果
     */
    public boolean checkCardIdUnique(Card card);
    /**
     * 查询账号是否已经有了校园卡
     *
     * @param card 校园卡信息
     * @return 结果
     */
    boolean checkCardNameUnique(Card card);
}
