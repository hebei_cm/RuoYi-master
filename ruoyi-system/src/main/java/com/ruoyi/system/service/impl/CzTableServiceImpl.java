package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CzTableMapper;
import com.ruoyi.system.domain.CzTable;
import com.ruoyi.system.service.ICzTableService;
import com.ruoyi.common.core.text.Convert;

/**
 * 充值Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-05
 */
@Service
public class CzTableServiceImpl implements ICzTableService 
{
    @Autowired
    private CzTableMapper czTableMapper;

    /**
     * 查询充值
     * 
     * @param rid 充值主键
     * @return 充值
     */
    @Override
    public CzTable selectCzTableByRid(Long rid)
    {
        return czTableMapper.selectCzTableByRid(rid);
    }

    /**
     * 查询充值列表
     * 
     * @param czTable 充值
     * @return 充值
     */
    @Override
    public List<CzTable> selectCzTableList(CzTable czTable)
    {
        return czTableMapper.selectCzTableList(czTable);
    }

    /**
     * 新增充值
     * 
     * @param czTable 充值
     * @return 结果
     */
    @Override
    public int insertCzTable(CzTable czTable)
    {
        return czTableMapper.insertCzTable(czTable);
    }

    /**
     * 修改充值
     * 
     * @param czTable 充值
     * @return 结果
     */
    @Override
    public int updateCzTable(CzTable czTable)
    {
        return czTableMapper.updateCzTable(czTable);
    }

    /**
     * 批量删除充值
     * 
     * @param rids 需要删除的充值主键
     * @return 结果
     */
    @Override
    public int deleteCzTableByRids(String rids)
    {
        return czTableMapper.deleteCzTableByRids(Convert.toStrArray(rids));
    }

    /**
     * 删除充值信息
     * 
     * @param rid 充值主键
     * @return 结果
     */
    @Override
    public int deleteCzTableByRid(Long rid)
    {
        return czTableMapper.deleteCzTableByRid(rid);
    }
}
