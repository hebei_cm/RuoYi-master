package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.CzTable;

/**
 * 充值Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-05
 */
public interface CzTableMapper 
{
    /**
     * 查询充值
     * 
     * @param rid 充值主键
     * @return 充值
     */
    public CzTable selectCzTableByRid(Long rid);

    /**
     * 查询充值列表
     * 
     * @param czTable 充值
     * @return 充值集合
     */
    public List<CzTable> selectCzTableList(CzTable czTable);

    /**
     * 新增充值
     * 
     * @param czTable 充值
     * @return 结果
     */
    public int insertCzTable(CzTable czTable);

    /**
     * 修改充值
     * 
     * @param czTable 充值
     * @return 结果
     */
    public int updateCzTable(CzTable czTable);

    /**
     * 删除充值
     * 
     * @param rid 充值主键
     * @return 结果
     */
    public int deleteCzTableByRid(Long rid);

    /**
     * 批量删除充值
     * 
     * @param rids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCzTableByRids(String[] rids);
}
