package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.XfTable;

import java.util.List;

/**
 * 消费Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-06
 */
public interface XfTableMapper 
{
    /**
     * 查询消费
     * 
     * @param cid 消费主键
     * @return 消费
     */
    public XfTable selectXfTableByCid(Long cid);

    /**
     * 查询消费列表
     * 
     * @param xfTable 消费
     * @return 消费集合
     */
    public List<XfTable> selectXfTableList(XfTable xfTable);

    /**
     * 新增消费
     * 
     * @param xfTable 消费
     * @return 结果
     */
    public int insertXfTable(XfTable xfTable);

    /**
     * 修改消费
     * 
     * @param xfTable 消费
     * @return 结果
     */
    public int updateXfTable(XfTable xfTable);

    /**
     * 删除消费
     * 
     * @param cid 消费主键
     * @return 结果
     */
    public int deleteXfTableByCid(Long cid);

    /**
     * 批量删除消费
     * 
     * @param cids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteXfTableByCids(String[] cids);
}
