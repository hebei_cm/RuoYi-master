package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 校园卡信息对象 card
 *
 * @author ruoyi
 * @date 2023-06-30
 */
public class Card extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 卡序号 */
    private Long cardNum;

    /** 卡号 */
    @Excel(name = "卡号")
    private String cardId;

    /** 余额 */
    @Excel(name = "余额")
    private String balance;

    /** 拥有人账号 */
    @Excel(name = "拥有人账号")
    private String cardName;

    public void setCardNum(Long cardNum)
    {
        this.cardNum = cardNum;
    }

    public Long getCardNum()
    {
        return cardNum;
    }
    public void setCardId(String cardId)
    {
        this.cardId = cardId;
    }

    public String getCardId()
    {
        return cardId;
    }
    public void setBalance(String balance)
    {
        this.balance = balance;
    }

    public String getBalance()
    {
        return balance;
    }
    public void setCardName(String cardName)
    {
        this.cardName = cardName;
    }

    public String getCardName()
    {
        return cardName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("cardNum", getCardNum())
                .append("cardId", getCardId())
                .append("balance", getBalance())
                .append("cardName", getCardName())
                .toString();
    }
}
