package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 消费对象 xf_table
 * 
 * @author ruoyi
 * @date 2023-07-06
 */
public class XfTable extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 消费序号 */
    private Long cid;

    /** 消费编号 */
    private String id;

    /** 消费金额 */
    @Excel(name = "消费金额")
    private String amount;

    /** 消费时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "消费时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date time;

    /** 卡号 */
    @Excel(name = "卡号")
    private String cardId;

    public void setCid(Long cid) 
    {
        this.cid = cid;
    }

    public Long getCid() 
    {
        return cid;
    }
    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setAmount(String amount)
    {
        this.amount = amount;
    }

    public String getAmount()
    {
        return amount;
    }
    public void setTime(Date time) 
    {
        this.time = time;
    }

    public Date getTime() 
    {
        return time;
    }
    public void setCardId(String cardId) 
    {
        this.cardId = cardId;
    }

    public String getCardId() 
    {
        return cardId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cid", getCid())
            .append("id", getId())
            .append("amount", getAmount())
            .append("time", getTime())
            .append("cardId", getCardId())
            .toString();
    }
}
