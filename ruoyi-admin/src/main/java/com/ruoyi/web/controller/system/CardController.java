package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.Card;
import com.ruoyi.system.service.ICardService;
import com.ruoyi.system.service.ISysUserService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 校园卡信息Controller
 * 
 * @author ruoyi
 * @date 2023-06-30
 */
@Controller
@RequestMapping("/system/card")
public class CardController extends BaseController
{
    private String prefix = "system/card";

    @Autowired
    private ICardService cardService;

    @Autowired
    private ISysUserService userService;

    @RequiresPermissions("system:card:view")
    @GetMapping()
    public String card()
    {
        return prefix + "/card";
    }

    /**
     * 查询校园卡信息列表
     */
    @RequiresPermissions("system:card:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Card card)
    {
        Long userId = ShiroUtils.getUserId();//当前登陆用户id
        SysUser sysUser=ShiroUtils.getSysUser();//获取用户对象
        String userName= sysUser.getLoginName();//获取用户登陆名
        System.out.println(userName);
        List<SysRole> roles=sysUser.getRoles();
        SysRole sysRole= roles.get(0);
        //普通角色id=101
        if (sysRole.getRoleId()==101)
        {
            card.setCardName(userName);
        }

        startPage();
        List<Card> list = cardService.selectCardList(card);
        return getDataTable(list);
    }

    /**
     * 导出校园卡信息列表
     */
    @RequiresPermissions("system:card:export")
    @Log(title = "校园卡信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Card card)
    {
        List<Card> list = cardService.selectCardList(card);
        ExcelUtil<Card> util = new ExcelUtil<Card>(Card.class);
        return util.exportExcel(list, "校园卡信息数据");
    }

    /**
     * 新增校园卡信息
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存校园卡信息
     */
    @RequiresPermissions("system:card:add")
    @Log(title = "校园卡信息", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Card card)
    {
        if(!cardService.checkCardIdUnique(card))
        {
            return error("新增卡号 '"+card.getCardId()+"' 失败，卡号已存在！");//检查卡号是否重复
        }
        if (userService.selectUserByLoginName(card.getCardName())==null)
        {
            return error("账号 '"+card.getCardName()+"' 不存在！");//检查账号是否存在
        }
        if(!cardService.checkCardNameUnique(card))
        {
            return error("账号 '"+card.getCardName()+"' 新增卡失败，已拥有卡！");//检查账号是否已经拥有卡
        }
        return toAjax(cardService.insertCard(card));
    }

    /**
     * 修改校园卡信息
     */
    @RequiresPermissions("system:card:edit")
    @GetMapping("/edit/{cardNum}")
    public String edit(@PathVariable("cardNum") Long cardNum, ModelMap mmap)
    {
        Card card = cardService.selectCardByCardNum(cardNum);
        mmap.put("card", card);
        return prefix + "/edit";
    }

    /**
     * 修改保存校园卡信息
     */
    @RequiresPermissions("system:card:edit")
    @Log(title = "校园卡信息", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Card card)
    {
        return toAjax(cardService.updateCard(card));
    }

    /**
     * 删除校园卡信息
     */
    @RequiresPermissions("system:card:remove")
    @Log(title = "校园卡信息", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(cardService.deleteCardByCardNums(ids));
    }
    /**
     * 根据拥有者名字查询校园卡
     */
    public Card selectCardBYCardName(String cardName)
    {
        return cardService.selectCardBYCardName(cardName);
    }
    /**
     * 根据卡号查询校园卡
     */
    public Card selectCardBYCardId(String cardId)
    {
        return cardService.selectCardBYCardId(cardId);
    }

}
