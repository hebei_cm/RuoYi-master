package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.Card;
import com.ruoyi.system.domain.XfTable;
import com.ruoyi.system.service.IXfTableService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * 消费Controller
 * 
 * @author ruoyi
 * @date 2023-07-06
 */
@Controller
@RequestMapping("/system/xf")
public class XfTableController extends BaseController
{
    private String prefix = "system/xf";
    @Autowired
    private CardController cardController;

    @Autowired
    private IXfTableService xfTableService;

    @RequiresPermissions("system:xf:view")
    @GetMapping()
    public String xf()
    {
        return prefix + "/xf";
    }

    /**
     * 查询消费列表
     */
    @RequiresPermissions("system:xf:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(XfTable xfTable)
    {
        SysUser sysUser= ShiroUtils.getSysUser();//获取用户对象
        String userName= sysUser.getLoginName();//获取用户账号
        List<SysRole> roles=sysUser.getRoles();//查询用户权限
        SysRole sysRole= roles.get(0);
        //普通角色id=101
        if (sysRole.getRoleId()==101)
        {
            String cardId =cardController.selectCardBYCardName(userName).getCardId();//根据用户名查询到对应的卡号
            System.out.println(cardId);
            xfTable.setCardId(cardId);
        }
        if (Objects.equals(xfTable.getId(), "所有")) {
            xfTable.setId(null);
        }
        startPage();
        List<XfTable> list = xfTableService.selectXfTableList(xfTable);
        return getDataTable(list);
    }

    /**
     * 导出消费列表
     */
    @RequiresPermissions("system:xf:export")
    @Log(title = "消费", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(XfTable xfTable)
    {
        List<XfTable> list = xfTableService.selectXfTableList(xfTable);
        ExcelUtil<XfTable> util = new ExcelUtil<XfTable>(XfTable.class);
        return util.exportExcel(list, "消费数据");
    }

    /**
     * 新增消费
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存消费
     */
    @RequiresPermissions("system:xf:add")
    @Log(title = "消费", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(XfTable xfTable)
    {
        SysUser sysUser= ShiroUtils.getSysUser();//获取用户对象
        String userName= sysUser.getLoginName();//获取用户账号
        List<SysRole> roles=sysUser.getRoles();//查询用户权限
        SysRole sysRole= roles.get(0);
        String amount=xfTable.getAmount();//获取消费的金额
        String cardID=xfTable.getCardId();//获取消费的卡号
        Card card=cardController.selectCardBYCardId(cardID);//根据卡号查询到卡
        if (cardController.selectCardBYCardId(cardID)==null)
        {
            return error("卡号 '"+cardID+"' 不存在！");//检查卡是否存在
        }
        if (sysRole.getRoleId()==101)
        {
            String cardId =cardController.selectCardBYCardName(userName).getCardId();//根据用户名查询到对应的卡号
            System.out.println(cardId);
            if (!Objects.equals(cardID, cardId))
            {
                return error("请输入正确的卡号！");
            }

        }
        if (Double.parseDouble(amount)<=0)
        {
            return error("消费金额必须为正数");
        }
        Double balance=Double.parseDouble(card.getBalance())-Double.parseDouble(amount);//消费后的余额
        if (balance<0)//检查余额是否足够消费
        {
            System.out.println("成功失败！");
            return error("消费失败余额不足！");
        }
        else{
            System.out.println("消费成功！");
            card.setBalance(Double.toString(balance));//修改余额
            cardController.editSave(card);
            return toAjax(xfTableService.insertXfTable(xfTable));
        }
    }

    /**
     * 修改消费
     */
    @RequiresPermissions("system:xf:edit")
    @GetMapping("/edit/{cid}")
    public String edit(@PathVariable("cid") Long cid, ModelMap mmap)
    {
        XfTable xfTable = xfTableService.selectXfTableByCid(cid);
        mmap.put("xfTable", xfTable);
        return prefix + "/edit";
    }

    /**
     * 修改保存消费
     */
    @RequiresPermissions("system:xf:edit")
    @Log(title = "消费", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(XfTable xfTable)
    {
        return toAjax(xfTableService.updateXfTable(xfTable));
    }

    /**
     * 删除消费
     */
    @RequiresPermissions("system:xf:remove")
    @Log(title = "消费", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(xfTableService.deleteXfTableByCids(ids));
    }
}
