package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.Card;
import com.ruoyi.system.domain.CzTable;
import com.ruoyi.system.service.ICzTableService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

/**
 * 充值Controller
 * 
 * @author ruoyi
 * @date 2023-07-05
 */
@Controller
@RequestMapping("/system/cz")
public class CzTableController extends BaseController
{
    private String prefix = "system/cz";


    @Autowired
    private CardController cardController;

    @Autowired
    private ICzTableService czTableService;

    @RequiresPermissions("system:cz:view")
    @GetMapping()
    public String cz()
    {
        return prefix + "/cz";
    }

    /**
     * 查询充值列表
     */
    @RequiresPermissions("system:cz:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CzTable czTable)
    {
        SysUser sysUser= ShiroUtils.getSysUser();//获取用户对象
        String userName= sysUser.getLoginName();//获取用户账号
        List<SysRole> roles=sysUser.getRoles();//查询用户权限
        SysRole sysRole= roles.get(0);
        //普通角色id=101
        if (sysRole.getRoleId()==101)
        {
            String cardId =cardController.selectCardBYCardName(userName).getCardId();//根据用户名查询到对应的卡号
            System.out.println(cardId);
            czTable.setCardId(cardId);
        }
        if (Objects.equals(czTable.getId(), "所有")) {
            czTable.setId(null);
        }
        startPage();
        List<CzTable> list = czTableService.selectCzTableList(czTable);
        return getDataTable(list);
    }

    /**
     * 导出充值列表
     */
    @RequiresPermissions("system:cz:export")
    @Log(title = "充值", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CzTable czTable)
    {
        List<CzTable> list = czTableService.selectCzTableList(czTable);
        ExcelUtil<CzTable> util = new ExcelUtil<CzTable>(CzTable.class);
        return util.exportExcel(list, "充值数据");
    }

    /**
     * 新增充值
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存充值
     */
    @RequiresPermissions("system:cz:add")
    @Log(title = "充值", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CzTable czTable)
    {
        SysUser sysUser= ShiroUtils.getSysUser();//获取用户对象
        String userName= sysUser.getLoginName();//获取用户账号
        List<SysRole> roles=sysUser.getRoles();//查询用户权限
        SysRole sysRole= roles.get(0);
        String amount=czTable.getAmount();//获取充值金额
        String cardID=czTable.getCardId();//获取充值卡号
        if (cardController.selectCardBYCardId(cardID)==null)
        {
            return error("卡号 '"+cardID+"' 不存在！");//检查卡是否存在
        }
        if (sysRole.getRoleId()==101)
        {
            String cardId =cardController.selectCardBYCardName(userName).getCardId();//根据用户名查询到对应的卡号
            System.out.println(cardId);
            if (!Objects.equals(cardID, cardId))//检查卡号是否是自己的卡号
            {
                return error("请输入正确的卡号！");
            }

        }
        System.out.println(cardID);
        Card card=cardController.selectCardBYCardId(cardID);//根据卡号查询到对应的卡
        Double balance=Double.parseDouble(card.getBalance())+Double.parseDouble(amount);//充值后的余额
        System.out.println(balance);
        card.setBalance(Double.toString(balance));//修改余额
        cardController.editSave(card);
        System.out.println("充值成功！");
        return toAjax(czTableService.insertCzTable(czTable));
    }

    /**
     * 修改充值
     */
    @RequiresPermissions("system:cz:edit")
    @GetMapping("/edit/{rid}")
    public String edit(@PathVariable("rid") Long rid, ModelMap mmap)
    {
        CzTable czTable = czTableService.selectCzTableByRid(rid);
        mmap.put("czTable", czTable);
        return prefix + "/edit";
    }

    /**
     * 修改保存充值
     */
    @RequiresPermissions("system:cz:edit")
    @Log(title = "充值", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CzTable czTable)
    {
        return toAjax(czTableService.updateCzTable(czTable));
    }

    /**
     * 删除充值
     */
    @RequiresPermissions("system:cz:remove")
    @Log(title = "充值", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(czTableService.deleteCzTableByRids(ids));
    }
}
